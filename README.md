# ***Alpha Ramble***
---

#### Vision Statement: 

*Alpha Ramble* is a mobile application for users who are lost with letters. Users who need guidance to figure out what words they find with the letters they have. It takes the letters out of your hands and returns to you words that can help you win cash like in the show of *Wheel of Fortune* or help you beat your friends in the board game *Scramble*. Maker letters better. User will be able to add some restrictions like “give me words that only have the letter ‘a’ in the second placement of the word”. *Alpha Ramble* will be the end to all scandals. Unlike other applications, *Alpha Ramble* will be able to provide you with points for *Scramble* and be able to scramble a word for you. This mobile application will be able to run on a cross platform development all platforms. 

#### Software Construction Process:

* Agile Software Construction
* Scrum Process

#### Tools:

* [BitBucket]("https://bitbucket.org/daniel_tapia/alpharamble/overview)
* Qt Creator 4.5.1 (Community)
* Azure

#### Release Date:

* May 31, 2018

#### Academic Excellence Showcase Presentation Poster:

![alt text](/Images/AlphaRamble_Poster.jpg "Alpha Ramble Poster")

* View More [Application Images](Images) 