import VPlay 2.0
import VPlayApps 1.0
import QtQuick 2.7
import QtQuick.Layouts 1.3
import QtPositioning 5.4
import QtQuick.XmlListModel 2.0

Page {
    id: scrabblePage
    backgroundColor: "#7fdbff"
    title: "Scrabble Points"
    // Ready to Run
    property bool isReady: false
    // Error Message
    property string errorMsg: ""
    // WordGameDictionary API Key
    property string wordAPIKey: ""
    // Data Model - Scrabble Word & Scrabble Points
    property var dataModel: []
    // Scrabble Pointer Tracker
    property int scrabbleScore: 0


    AppText { // Title Text
        id: titleText
        y: 25
        color: "#563517"
        text: "Scrabble Scorer!"
        font.pixelSize: 40
        font.underline: false
        font.family: "Times New Roman"
        font.italic: true
        font.bold: true
        verticalAlignment: Text.AlignTop
        textFormat: Text.AutoText
        anchors.horizontalCenter: parent.horizontalCenter
    }

    IconButton { // Info Icon
        id: iButton
        y: 25
        x: titleText.x + titleText.width + 10
        icon: IconType.infocircle
        color: "#aaaaaa"
        toggle: true

        onToggled: {
          NativeDialog.confirm("Information", "Enter a Scrabble word to view the word's Scrabble points", function(ok) {
          if(ok) {
            console.log("Info Read");
          }
        })
        }
      }

    Row { // Row Layout: Text, TextField, Button
        id: row
        y: 100
        width: scrabblePage.width - 20
        height: 30
        spacing: 10
        anchors.horizontalCenter: parent.horizontalCenter


        AppText { // Text: Enter a Word
            id: scrabbleText
            width: 145
            height: row.height
            text: qsTr("Enter a Word:")
            font.pixelSize: 22
            font.underline: true
        }

        AppTextField { // User Word Input
            id: scrabbleTextField
            width: 100
            height: row.height
            placeholderText: "ex: ramble"
            clearsOnBeginEditing: true
            showClearButton : true
        }

        AppButton { // Submit Button
            id: submitButton
            height: row.height
            text: "Submit"
            textColor: "#ffffff"
            backgroundColor: "#00507f"
            flat: false
            enabled: true
            textSize: 15
            fontBold: true
            onClicked: { // Load Data
                // Page Ready to Run
                scrabblePage.isReady = true;
                // Clear Error Message
                scrabblePage.errorMsg = "";
                // Checks if word is valid
                scrabblePage.checkword(scrabbleTextField.text);
            }
        }
    } // Row

    // Checks if Word is Valid
    function checkword(word) {
        word = word.replace(/\s/g,''); // eliminate white spaces
        console.log("Word Checking: " + word);
        if (word.length > 1) { // If user word greater than one
            // Set API Key
            scrabblePage.wordAPIKey = "?key=1.0724411209996222e30";
            xmlModel.word = word; // Add word to API Call for Scrabble Points
        } else { // Else add more letters
            scrabblePage.errorMsg = "Please enter two letters";
        }
    }


    XmlListModel { // Model for loading and parsing xml data
        id: xmlModel
        // API Endpoint
        property string endpoint: "https://www.wordgamedictionary.com/api/v1/references/scrabble/"
        // Word for Points
        property string word: ""
        // Sets GET Request to API  // Set xml source to load data from local file or web service
        source: endpoint + word + scrabblePage.wordAPIKey
        // Set query that returns items
        query: "/entry"
        // Specify roles to access item data
        XmlRole { name: "word"; query: "word/string()"}
        XmlRole { name: "scrabblescore"; query: "scrabblescore/string()"}
    }

    AppListView {// Display the xml model in a list
        id: listView
        width: scrabblePage.width
        anchors.bottomMargin: 10
        anchors.bottom: column.top
        anchors.topMargin: 10
        anchors.top: error.bottom
        anchors.left: row.left
        anchors.right: row.right
        visible: true
        backgroundColor: "#7fdbff"
        model: scrabblePage.dataModel // Get data model
        // Display Model onto SimpleRow
        delegate: SimpleRow { enabled: false } // Disable click events
    }

    ListPage {
        id: scrabbleListPage
        width: scrabblePage.width
        anchors.bottomMargin: 10
        anchors.bottom: column.top
        anchors.topMargin: 10
        anchors.top: error.bottom
        visible: false //Not Visible
        backgroundColor: "#7fdbff"
        title: "Parse XML"
        model: xmlModel
        emptyText.text: "Not a Scrabble Word" //If no Words are found from API Call
        delegate: SimpleRow {
            id: scrabbleWord
            // Text Layout
            text: "Word: " + word + ",  Scrabble Score: " + scrabblescore
            // Scrabble Score string
            property string scrabbleScore: scrabblescore
            // Scrabble Word string
            property string scrabbleWord: word
            onTextChanged: { // Submit action listener
                if (scrabblePage.isReady === true) { // Checks if app is fully loaded
                    if (scrabbleWord.scrabbleScore === "0") { // If Score equals to 0
                        scrabblePage.errorMsg = "Not a Scrabble Word!";
                    } else { // Else push word to the data model
                        var word = scrabbleWord.text;
                        // LowerCase the word
                        word = word.toLowerCase();
                        // Gets word & points                        
                        var newItem = { text: word }
                        // Push new Item onto the data model
                        scrabblePage.dataModel.push(newItem);
                        // Reverse order, newer Item on top
                        scrabblePage.dataModel.reverse();
                        // signal change in data model to trigger UI update (list view)
                        scrabblePage.dataModelChanged()
                        // Reverse order back to normal for next new Item
                        scrabblePage.dataModel.reverse();
                        // Clear APIKEY
                        scrabblePage.wordAPIKey = "";
                    }
                } else { // Else App is not fully loaded
                    console.log("Not Ready: ScrabblePointsPage");
                }
            } // End of Action Listener
        } // SimpleRow
    } // ListPage

    // Display Data Model

    Column {
        id: column
        y: 400
        anchors.bottomMargin: 25
        anchors.bottom: parent.bottom
        spacing: 10
        anchors.horizontalCenter: parent.horizontalCenter

        IconButton {
            id: infoButton
            icon: IconType.infocircle
            color: "#aaaaaa"
            toggle: true
            onToggled: {
              NativeDialog.confirm("Information", "Use Slider to set points. You can add or reset points!", function(ok) {
              if(ok) {
                console.log("Read Info");
              }
            })
            }
          }

        AppSlider { // Scrabble Point Slider
            id: scrabbleSlider
            knobShadow: true
        }

        AppText { // Slider Points Text
            text: "Points: " + Math.round(scrabbleSlider.position * 100 / 2)
            horizontalAlignment: Text.AlignHCenter
            width: parent.width
        }

        AppButton{ // Add Points Button
            id: addButton
            text: "Add Points"
            textColor: "#ffffff"
            width: column.width
            backgroundColor: "#008709"
            enabled: true
            textSize: 15
            fontBold: true
            flat: false
            onClicked: {
                var points = scrabblePage.scrabbleScore
                scrabblePage.scrabbleScore = points + Math.round(scrabbleSlider.position * 100 / 2)
            }
        }

        AppText { // Scrabble Points Text
            id: scoreText
            text: "Scrabble Points: " + scrabblePage.scrabbleScore
            horizontalAlignment: Text.AlignHCenter
            width: parent.width
        }

        AppButton { // Reset Scrabble Point Button
            id: resetButton
            text: "Reset Score"
            textColor: "#ffffff"
            width: column.width
            backgroundColor: "#ba2121"
            enabled: true
            textSize: 15
            fontBold: true
            flat: false
            onClicked: {
                scrabblePage.scrabbleScore = 0
            }
        }
    } // Column

    AppText { //Error Message
        id: error
        width: row.width
        color: "#c10808"
        text: scrabblePage.errorMsg
        horizontalAlignment: Text.AlignHCenter
        font.pixelSize: 20
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: row.bottom
        anchors.topMargin: 5
    }
} // Scrabble Page
