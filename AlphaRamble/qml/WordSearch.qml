import VPlay 2.0
import VPlayApps 1.0
import QtQuick 2.7
import QtQuick.Layouts 1.3
import QtPositioning 5.4
import QtQuick.XmlListModel 2.0


Page { //Search Page
    id: wordSearchPage
    backgroundColor: "#7fdbff"
    title: "Word Search"
    // App Ready to Run
    property bool isReady: false;
    // Data Model - Words found
    property var dataModel: [];
    // Error Message
    property string errorMsg: ""
    // WordGameDictionary API Key
    property string wordAPIKey: "";
    // Credit: https://stackoverflow.com/questions/40120915/javascript-function-that-returns-true-if-a-letter
    // Check if letter is aplhabetical
    property var isAlpha: function(ch){
        return typeof ch === "string" //&& ch.length === 1
                && (ch >= "a" && ch <= "z" || ch >= "A" && ch <= "Z");
    }

    AppText { // Title Text
        id: titleText
        y: 25
        color: "#563517"
        text: "Word Search!"
        font.pixelSize: 50
        font.underline: false
        font.family: "Times New Roman"
        font.italic: true
        font.bold: true
        verticalAlignment: Text.AlignTop
        textFormat: Text.AutoText
        anchors.horizontalCenter: parent.horizontalCenter
    }


    IconButton { // Info Icon
        id: iconButton
        y: 25
        x: titleText.x + titleText.width + 10
        icon: IconType.infocircle
        color: "#aaaaaa"
        toggle: true

        onToggled: {
            NativeDialog.confirm("Information", "Enter letters to return words that can be found in with thoses letters!", function(ok) {
                if(ok) {
                    console.log("Info Read");
                }
            })
        }
    }


    Row { // Row0 Layout: Text, TextField, Button
        id: row0
        y: 100
        width: wordSearchPage.width - 20
        height: 30
        spacing: 10
        anchors.horizontalCenter: parent.horizontalCenter


        AppText { // Text: Enter a Word
            id: scrabbleText
            width: 145
            height: row0.height
            text: qsTr("Enter Letters:")
            font.pixelSize: 22
            font.underline: true
        }

        AppTextField { // User Letters Input
            id: scrabbleTextField
            width: 100
            height: row0.height
            placeholderText: "ex: mrblae"
            clearsOnBeginEditing: false
            showClearButton : true
        }

        AppButton { // Submit Button
            id: submitButton
            height: row0.height
            text: "Submit"
            textColor: "#ffffff"
            backgroundColor: "#00507f"
            flat: false
            enabled: true
            textSize: 15
            fontBold: true
            onClicked: {
                // Page Ready to Run
                wordSearchPage.isReady = true;
                // Clear Error Message
                wordSearchPage.errorMsg = "";
                // Clear API Key
                wordSearchPage.wordAPIKey = "";
                //wordListPage2.visible = false;
                if(wordSearchPage.isSpecified()){
                    // Checks if letters are valid
                    wordSearchPage.checkletters(scrabbleTextField.text);
                }
            }
        }
    } // Row0

    function isSpecified(){
        if (specificLetterText.text === "" && positionText.text === "") {
            console.log("No Specifications");
            return true;
        } else if (specificLetterText.text !== "" && positionText.text !== ""){
            console.log("Letter & Postion Specified");
            return true;
        } else {
            console.log("Only one field filled in");
            wordSearchPage.errorMsg = "Enter both 'Specific Letter' & 'Letter Position'";
            return false;
        }
    }

    function checkletters(letters) { // Checks User Input Letters
        letters = letters.replace(/\s/g,''); //eliminate white spaces
        letters = letters.toLowerCase() //lowercase letters
        console.log("User Input Letters: " + letters);
        if(letters === ""){
            wordSearchPage.errorMsg = "Nothing to Execute";
            wordListPage2.visible = false;
        }else if (wordSearchPage.isAlpha(letters) === false){
            wordSearchPage.errorMsg = "Only enter Letters";
            wordListPage2.visible = false;
        }else if(letters.length === 1) {
            wordSearchPage.errorMsg = "Add another Letter";
            wordListPage2.visible = false;
        }else {
            // Emply Data Model
            wordSearchPage.dataModel = [];
            // Set API Key
            wordSearchPage.wordAPIKey = "";
            console.log("new XML Request!");
            xmlModel.alpha = letters; //API Call to look for words
            wordListPage2.visible = true; //Word List Visible
        }
    }

    Row {
        id: row1
        width: parent.verticalCenter
        height: 30
        anchors.topMargin: 10
        anchors.top: row0.bottom
        spacing: 5
        anchors.horizontalCenter: parent.horizontalCenter

        StyledButton { // Button to add Specific Letter
            text: "Specific Letter"
            onClicked: InputDialog.inputTextSingleLine(rambleApp,
                                                       "Enter a Specific Letter", //message text
                                                       "ex: 'r'", //placeholder text
                                                       function(ok, preciseLetter) {
                                                           if (ok) {
                                                               if (wordSearchPage.checkletter(preciseLetter) === true) {
                                                                   console.log("Specific Letter Change");
                                                                   specificLetterText.text = preciseLetter;
                                                               } else if (preciseLetter === "") {
                                                                   console.log("Specific Letter Cleared");
                                                                   specificLetterText.text = preciseLetter;
                                                               }
                                                           }
                                                       })
        }

        AppText{ // Display Specific Letter
            id: specificLetterText
            height: row2.height
            color: "#323b32"
            width: 20
            font.pixelSize: 25
            font.underline: false
            text: ""
        }

        StyledButton { // Button to add Ending Letter
            text: "At Position"
            onClicked: InputDialog.inputTextSingleLine(rambleApp,
                                                       "Enter a Specific Position", //message text
                                                       "ex: '3'", //placeholder text
                                                       function(ok, letterPosition) {
                                                           if (ok) {
                                                               wordSearchPage.errorMsg = "";
                                                               if (letterPosition.length > 2) {
                                                                   console.log("Letter Position to High!");
                                                                   wordSearchPage.errorMsg = "Only Enter Two Numbers";
                                                               } else if (letterPosition === "") {
                                                                   console.log("Clear Position");
                                                                   positionText.text = letterPosition;
                                                               } else if (parseInt(letterPosition) < 1) { // Checks if its an interger
                                                                   console.log("Position less than 1");
                                                                   wordSearchPage.errorMsg = "Enter a Position Greater than Zero";
                                                               } else if (parseInt(letterPosition)) { // Checks if its an interger
                                                                   console.log("Valid Letter Position");
                                                                   positionText.text = letterPosition;
                                                               } else {
                                                                   console.log("Not Valid Position");
                                                                   wordSearchPage.errorMsg = "'" + letterPosition + "' is Not a number";
                                                               }
                                                           }
                                                       })
        }

        AppText{ // Display Word Length
            id: positionText
            height: row2.height
            color: "#323b32"
            width: 20
            font.pixelSize: 25
            font.underline: false
            text: ""
        }
    } // Row1

    Row { // Row2 Layout: Button, Text
        id: row2
        width: parent.verticalCenter
        height: 30
        anchors.topMargin: 10
        anchors.top: row1.bottom
        spacing: 5
        anchors.horizontalCenter: parent.horizontalCenter


        StyledButton { // Button to add Starting Letter
            text: "Start With"
            onClicked: InputDialog.inputTextSingleLine(rambleApp,
                                                       "Enter First Letter", //message text
                                                       "ex: 'a'", //placeholder text
                                                       function(ok, startLetter) {
                                                           if (ok) {
                                                               if (wordSearchPage.checkletter(startLetter) === true) {
                                                                   console.log("First Letter Change");
                                                                   firstLetterText.text = startLetter;
                                                               } else if (startLetter === "") {
                                                                   console.log("Clear Letter");
                                                                   firstLetterText.text = startLetter;
                                                               }
                                                           }
                                                       })
        }

        AppText{ // Display Starting Letter
            id: firstLetterText
            height: row2.height
            color: "#323b32"
            width: 20
            font.pixelSize: 25
            font.underline: false
            text: ""
        }

        StyledButton { // Button to add Ending Letter
            text: "End With"
            onClicked: InputDialog.inputTextSingleLine(rambleApp,
                                                       "Enter Last Letter", //message text
                                                       "ex: 'z'", //placeholder text
                                                       function(ok, endLetter) {
                                                           if (ok) {
                                                               if (wordSearchPage.checkletter(endLetter) === true) {
                                                                   console.log("Last Letter Change");
                                                                   lastLetterText.text = endLetter;
                                                               } else if (endLetter === "") {
                                                                   console.log("Clear Letter");
                                                                   lastLetterText.text = endLetter;
                                                               }
                                                           }
                                                       })
        }


        AppText{ // Display Ending Letter
            id: lastLetterText
            height: row2.height
            color: "#323b32"
            width: 20
            font.pixelSize: 25
            font.underline: false
            text: ""
        }

        StyledButton { // Button to set Word Length
            text: "Word Length"
            onClicked: InputDialog.inputTextSingleLine(rambleApp,
                                                       "Enter Word Length", //message text
                                                       "ex: '5'", //placeholder text
                                                       function(ok, wordLength) {
                                                           if (ok) {
                                                               wordSearchPage.errorMsg = "";
                                                               if( wordLength.length > 2){
                                                                   console.log("Too big of word lenght");
                                                                   wordSearchPage.errorMsg = "No Word is that Long";
                                                               } else if (wordLength === "") {
                                                                   console.log("Clear Length");
                                                                   lengthText.text = wordLength;
                                                               } else if (parseInt(wordLength) < 2) { // Checks if its an interger
                                                                   console.log("Length less than 2");
                                                                   wordSearchPage.errorMsg = "Enter a Length Value Greater than Two";
                                                               } else if (parseInt(wordLength)) { // Checks if its an interger
                                                                   console.log("Valid Length");
                                                                   lengthText.text = wordLength;
                                                               } else {
                                                                   console.log("Not Valid Length");
                                                                   wordSearchPage.errorMsg = "'" + wordLength + "' is Not a number";
                                                               }
                                                           }
                                                       })
        }

        AppText{ // Display Word Length
            id: lengthText
            height: row2.height
            color: "#323b32"
            width: 20
            font.pixelSize: 25
            font.underline: false
            text: ""
        }
    } // Row2

    function checkletter(letter) { // Check Starting/Ending Letter
        console.log("Checking Letter: " + letter);
        wordSearchPage.errorMsg = "";
        if (letter === "") {
            return false;
        }
        letter = letter.replace(/\s/g,''); //eliminate white spaces
        if (letter.length > 1) {
            wordSearchPage.errorMsg = "Only enter one letter!";
            return false;
        } else if (wordSearchPage.isAlpha(letter) === false) { // if letter is alphabetic
            wordSearchPage.errorMsg = "'" + letter + "' is Not a Letter!";
            return false;
        } else if (letter.length === 1) {
            letter = letter.toLowerCase(); // Lowercase letter
            return true;
        }
    }

    //Credits:
    //https://stackoverflow.com/questions/29189248/multiple-nested-levels-in-a-qml-xmllistmodel?rq=1
    //https://v-play.net/doc/apps-howtos/#read-and-parse-xml
    //http://doc.qt.io/qt-5/qml-qtquick-xmllistmodel-xmllistmodel.html
    XmlListModel { // model for loading and parsing xml data
        id: xmlModel
        // API Endpoint for words within the letters
        property string endpoint: "https://www.wordgamedictionary.com/api/v1/references/anagrams/";
        // User Input Letters
        property string alpha: "";
        // set xml source to load data from local file or web service
        source: endpoint + alpha + wordSearchPage.wordAPIKey
        // set query that returns items
        query: "/entry/word"
        // specify roles to access item data
        XmlRole { name: "wordText"; query: "string()"; }
    }//XML


    ListPage { // List of all Words
        id: wordListPage
        visible: false
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 100
        anchors.top: row2.bottom
        anchors.topMargin: 30
        backgroundColor: "#7fdbff"
        title: "Parse XML"
        model: xmlModel
        //Credit: https://v-play.net/doc/vplayapps-simplerow/#default-usage
        // How each row will be displayed as
        delegate: SimpleRow {
            id: wordOutput
            text: wordText
            enabled: false
            // Scrabble Word string
            property string scrabbleWord: wordText;
            onTextChanged: {
                if (wordSearchPage.isReady === true) {
                    // Get Values
                    var word = wordOutput.text;
                    var start = firstLetterText.text;
                    var end = lastLetterText.text;
                    var wordLength = lengthText.text;
                    var letter = specificLetterText.text;
                    var letterPosition = positionText.text;
                    if (letter !== "" && letterPosition !== ""){
                        letterPosition = parseInt(letterPosition);
                        letterPosition = letterPosition - 1;
                        if (word.charAt(letterPosition) === letter) {
                            if (wordLength === "") {
                                addWord(start,end,word);
                            } else {
                                wordLength = parseInt(wordLength);
                                if (wordLength === word.length) {
                                    addWord(start,end,word);
                                }
                            }
                        }
                    } else {
                        if (wordLength === "") {
                            addWord(start,end,word);
                        } else {
                            wordLength = parseInt(wordLength);
                            if (wordLength === word.length) {
                                addWord(start,end,word);
                            }
                        }
                    }
                } else { // Else App is not fully loaded
                    console.log("Page Not Ready: WordSearchPage");
                }
            }

        } // SimpleRow: wordOutput
    }//List

    function addWord(start,end,word){
        if (start !== "" && end !== "") {
            if(word.charAt(0) === start && word.charAt(word.length - 1) === end) {
                addToDataModel(word);
            }
        } else if (start !== "" && end === "") {
            if(word.charAt(0) === start) {
                addToDataModel(word);
            }
        } else if (start === "" && end !== "") {
            if(word.charAt(word.length - 1) === end) {
                addToDataModel(word);
            }
        } else {
            addToDataModel(word);
        }
    }

    function addToDataModel(word) { // Add Word to DataModel - Data Model Change!
        // Get word string
        var newItem = { text: word };
        // Push new Item onto the data model
        wordSearchPage.dataModel.push(newItem);
        // Reverse order, newer Item on top
        wordSearchPage.dataModel.reverse();
        // signal change in data model to trigger UI update (list view)
        wordSearchPage.dataModelChanged();
        // Reverse order back to normal for next new Item
        wordSearchPage.dataModel.reverse();
    }

    ListPage { //Display results
        id: wordListPage2
        visible: false
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 100
        anchors.top: row2.bottom
        anchors.topMargin: 30
        backgroundColor: "#7fdbff"
        title: "Parse XML"
        model: wordSearchPage.dataModel
        emptyText.text: "No Words Found :(" //If no Words are found from API Call
        //Credit: https://v-play.net/doc/vplayapps-simplerow/#default-usage
        // How each row will be displayed as
        //delegate: SimpleRow { enabled: false } // Display Results!
        delegate: SimpleRow {
            id: defWordOutput
            onSelected: {
                getSelectedWord(defWordOutput.text); //Go Defind the Word
            }
        } //defWord
    }//List

    function getSelectedWord(define){//Word to define
        console.log("Word to Defind: " + define);
        defXMLModel.defWord = define; //Makes API call
        titleText.text = define; //Set Title to Word Being Defined
        defPage.visible = true; //Definition List Visible
        backButton.visible = true; // Back Button Visible
        // Set to Not visible
        wordListPage.visible = false;
        wordListPage2.visible = false;
        iconButton.visible = false;
        row0.visible = false;
        row1.visible = false;
        row2.visible = false;
    }


    XmlListModel { // model for loading and parsing xml data
        id: defXMLModel
        // API Endpoint for definding words
        property string endpoint: "https://www.wordgamedictionary.com/api/v1/references/definitions/"
        // Word to define
        property string defWord: ""
        // set xml source to load data from local file or web service
        source: endpoint + defWord + wordSearchPage.wordAPIKey
        // set query that returns items
        query: "/entry/data/definitions/def"
        // specify roles to access item data
        XmlRole { name: "definition"; query: "string()"}
    }//XML


    ListPage { //Display Word Definition
        id: defPage
        visible: false
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 100
        anchors.top: row2.bottom
        anchors.topMargin: 30
        backgroundColor: "#7fdbff"
        model: defXMLModel
        emptyText.text: "No Definition :("
        delegate: SimpleRow {
            id: defRow
            detailText: definition
            enabled: false
        } //Definition
    }//List

    StyledButton{ //Back button to Word List
        id: backButton
        width: 150
        height: 30
        text: "<- Back to List"
        visible: false
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: row2.bottom
        anchors.topMargin: -25
        onClicked: {
            defPage.visible = false; // Definition Table not visible
            backButton.visible = false; // Button not visible
            // Set to  visible
            wordListPage2.visible = true;
            iconButton.visible = true;
            row0.visible = true;
            row1.visible = true;
            row2.visible = true;
            titleText.text = "Word Search"; //Set Title Back
        }
    }


    AppText { //Error Message Text
        id: error
        width: row2.width
        color: "#c10808"
        text: wordSearchPage.errorMsg
        horizontalAlignment: Text.AlignHCenter
        font.pixelSize: 20
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: row2.bottom
        anchors.topMargin: 5
    }


} // End of Search Page

