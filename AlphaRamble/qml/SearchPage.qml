import VPlay 2.0
import VPlayApps 1.0
import QtQuick 2.7
import QtQuick.Layouts 1.3
import QtPositioning 5.4
import QtQuick.XmlListModel 2.0

Page { //Search Page
    id: searchPage
    width: 436
    height: 824
    backgroundColor: "#7fdbff"
    title: "Alpha Search"
    // Error Message
    property string errorMsg: ""
    // WordGameDictionary API Key
    property string wordAPIKey: ""

    AppText { //Title
        id: titleText
        y: 25
        color: "#584937"
        text: "Alpha Ramble!"
        font.pixelSize: 40
        font.underline: false
        font.family: "Times New Roman"
        font.italic: true
        font.bold: true
        verticalAlignment: Text.AlignTop
        textFormat: Text.AutoText
        anchors.horizontalCenter: parent.horizontalCenter
    }


    Row { //Row Layout: alphaText, rambleTextField, submitButton
        id: row
        y: 100
        width: searchPage.width - 50
        height: 30
        spacing: 20
        anchors.horizontalCenter: parent.horizontalCenter


        AppText { //Text: Enter Letters
            id: alphaText
            width: 135
            height: row.height
            text: qsTr("Enter Letters:")
            font.pixelSize: 22
            font.underline: true
        }

        AppTextField { //User Input
            id: rambleTextField
            width: 100
            height: row.height
            placeholderText: "ex: rmaleb"
            clearsOnBeginEditing: true
            showClearButton : true
        }

        AppButton { //Submit Button
            id: submitButton
            height: row.height
            text: "Submit"
            flat: false
            enabled: true
            textSize: 15
            fontBold: true
            onClicked: { //Load Data
                //Clear Error Message
                searchPage.errorMsg = ""
                //Checks if word is greater than a letter
                searchPage.checkletters(rambleTextField.text)
            }
        }
    }//Row


    function checkletters(letters){ //Checks if User word is greater than a one
        letters = letters.replace(/\s/g,'') //eliminate white spaces
        console.log("Letters Checking: " + letters)
        if(letters.length > 1){ //If greater than one, execute xml request
            xmlModel.alpha = letters //API Call to look for words
            wordListPage.visible = true //Word List Visible
        }
        else if(letters.length === 1) {
            searchPage.errorMsg = "Add another Letter"
            wordListPage.visible = false
        }
        else { //Else display error message
            searchPage.errorMsg = "Nothing to Execute"
            wordListPage.visible = false
        }
    }

    //Credits:
    //https://stackoverflow.com/questions/29189248/multiple-nested-levels-in-a-qml-xmllistmodel?rq=1
    //https://v-play.net/doc/apps-howtos/#read-and-parse-xml
    //http://doc.qt.io/qt-5/qml-qtquick-xmllistmodel-xmllistmodel.html
    XmlListModel { // model for loading and parsing xml data
        id: xmlModel
        // API Endpoint for words within the letters
        property string endpoint: "https://www.wordgamedictionary.com/api/v1/references/anagrams/"
        // User Input Letters
        property string alpha: ""
        // set xml source to load data from local file or web service
        source: endpoint + alpha + searchPage.wordAPIKey
        // set query that returns items
        query: "/entry/word"
        // specify roles to access item data
        XmlRole { name: "wordText"; query: "string()"}
        //XmlRole { name: "scrabblescore"; query: "scrabblescore/string()"}
    }//XML


    ListPage { //Display results
        id: wordListPage
        visible: false
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 100
        anchors.top: row.bottom
        anchors.topMargin: 30
        backgroundColor: "#7fdbff"
        title: "Parse XML"
        model: xmlModel
        emptyText.text: "No Words Found" //If no Words are found from API Call
        //Credit: https://v-play.net/doc/vplayapps-simplerow/#default-usage
        // How each row will be displayed as
        delegate: SimpleRow {
            id: wordOutput
            text: wordText
            onSelected: {
                getSelectedWord(wordOutput.text) //Go Defind the Word
            }
        } //wordOutput
    }//List

    function getSelectedWord(define){//Word to define
        console.log("Word to Defind: " + define)
        defXMLModel.defWord = define //Makes API call
        titleText.text = define //Set Title to Word Being Defined
        defPage.visible = true //Definition List Visible
        backButton.visible = true // Back Button Visible
        row.visible = false //Search Option not Visible
    }


    XmlListModel { // model for loading and parsing xml data
        id: defXMLModel
        // API Endpoint for definding words
        property string endpoint: "https://www.wordgamedictionary.com/api/v1/references/definitions/"
        // Word to define
        property string defWord: ""
        // set xml source to load data from local file or web service
        source: endpoint + defWord + searchPage.wordAPIKey
        // set query that returns items
        query: "/entry/data/definitions/def"
        // specify roles to access item data
        XmlRole { name: "definition"; query: "string()"}
    }//XML


    ListPage { //Display Word Definition
        id: defPage
        visible: false
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 100
        anchors.top: row.bottom
        anchors.topMargin: 30
        backgroundColor: "#7fdbff"
        model: defXMLModel
        delegate: SimpleRow {
            id: defRow
            detailText: definition
            enabled: false
        } //Definition
    }//List

    AppButton{ //Back button to Word List
        id: backButton
        width: 150
        height: 20
        text: "<- Back to List"
        flat: true
        visible: false
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: row.bottom
        anchors.topMargin: -25
        onClicked: {
            defPage.visible = false // Definition Table not visible
            backButton.visible = false // Button not visible
            row.visible = true // Search Text Field Visible
            titleText.text = "Alpha Search" //Set Title Back
        }
    }


    AppText { //Error Message
        id: error
        width: row.width
        color: "#c10808"
        text: searchPage.errorMsg
        horizontalAlignment: Text.AlignHCenter
        font.pixelSize: 20
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: row.bottom
        anchors.topMargin: 5
    }


//    // Hack way to Get the api key from outside project
//    XmlListModel{
//        id: apiModel
//        source: "../../../WordGameDictionaryKey.xml"
//        query: "/entry/key"
//        XmlRole { name: "key"; query: "key/string()"}
//    }

//    ListPage { // GET API KEY
//        id: apiListPage
//        anchors.bottom: parent.bottom
//        anchors.bottomMargin: 600
//        anchors.top: row.bottom
//        anchors.topMargin: 30
//        backgroundColor: "#7fdbff"
//        title: "KEY"
//        visible: true
//        model: apiModel
//        delegate: SimpleRow {
//            id: apiSimpleRow
//            text: key
//            onSelected: {
//                searchPage.wordAPIKey = apiSimpleRow.text
//            }
//        }
//    } // GET API KEY

} // End of Search Page
