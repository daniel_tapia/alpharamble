import VPlay 2.0
import VPlayApps 1.0
import QtQuick 2.7
import QtQuick.Layouts 1.3
import QtPositioning 5.4
import QtQuick.XmlListModel 2.0

Page { //Search Page
    id: aboutPage
    backgroundColor: "#7fdbff"
    title: "Word Search"

    AppText { //Title
        id: titleText
        y: 25
        color: "#563517"
        text: "About Page!"
        font.pixelSize: 50
        font.underline: false
        font.family: "Times New Roman"
        font.italic: true
        font.bold: true
        verticalAlignment: Text.AlignTop
        anchors.horizontalCenter: parent.horizontalCenter
    }

    //***************************************AlphaRamble************************************//

    AppText {
        id: textAlphaRamble
        text: "Alpha Ramble"
        font.underline: true
        anchors.top: titleText.bottom
        anchors.topMargin: 25
        font.family: "Times New Roman"
        width: aboutPage.width
        font.pixelSize: 25
        font.bold: true
        horizontalAlignment: Text.AlignHCenter
    }

    AppText {
        id: infoTextAlphaRamble
        width: aboutPage.width
        text: "“Word Search” - For those who are looking for \n"
              + "words within letters, there you'll be able \n"
              + "to sort words by using the features there. \n \n"
              + "“Scrabble Scorer” - Go find out how many \n"
              + "points your word is worth. Be sure to \n"
              + "keep track of points by using the slider"
        font.family: "Verdana"
        font.italic: true
        anchors.top: textAlphaRamble.bottom
        anchors.topMargin: 10
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignTop
        font.pixelSize: 15
    }

    //***************************************PlatForm************************************//

    AppText {
        id: textPlatForm
        text: "Platform"
        font.underline: true
        anchors.top: infoTextAlphaRamble.bottom
        anchors.topMargin: 10
        font.family: "Times New Roman"
        width: aboutPage.width
        font.pixelSize: 25
        font.bold: true
        horizontalAlignment: Text.AlignHCenter
    }

    AppText {
        id: infoTextPlatForm
        width: aboutPage.width
        text: "V-Play Engine"
        font.family: "Verdana"
        font.italic: true
        anchors.top: textPlatForm.bottom
        anchors.topMargin: 5
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignTop
        font.pixelSize: 15
    }

    //***************************************FrameWork************************************//

    AppText {
        id: textFrameWork
        text: "Framework"
        font.underline: true
        anchors.top: infoTextPlatForm.bottom
        anchors.topMargin: 10
        font.family: "Times New Roman"
        width: aboutPage.width
        font.pixelSize: 25
        font.bold: true
        horizontalAlignment: Text.AlignHCenter
    }

    AppText {
        id: infoTextFrameWork
        width: aboutPage.width
        text: "Qt Creator 4.5.1 (Community)"
        font.family: "Verdana"
        font.italic: true
        anchors.top: textFrameWork.bottom
        anchors.topMargin: 5
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignTop
        font.pixelSize: 15
    }

    //***************************************Languages************************************//

    AppText {
        id: textLanguages
        text: "Languages"
        font.underline: true
        anchors.top: infoTextFrameWork.bottom
        anchors.topMargin: 10
        font.family: "Times New Roman"
        width: aboutPage.width
        font.pixelSize: 25
        font.bold: true
        horizontalAlignment: Text.AlignHCenter
    }

    AppText {
        id: infoTextLanguages
        width: aboutPage.width
        text: "QML, Javascript, C++"
        font.family: "Verdana"
        font.italic: true
        anchors.top: textLanguages.bottom
        anchors.topMargin: 5
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignTop
        font.pixelSize: 15
    }

    //***************************************Developer************************************//

    AppText {
        id: textDeveloper
        text: "Developer"
        font.underline: true
        anchors.top: infoTextLanguages.bottom
        anchors.topMargin: 10
        font.family: "Times New Roman"
        width: aboutPage.width
        font.pixelSize: 25
        font.bold: true
        horizontalAlignment: Text.AlignHCenter
    }

    AppText {
        id: infoTextDeveloper
        width: aboutPage.width
        text: "Daniel Tapia"
        font.family: "Verdana"
        font.italic: true
        anchors.top: textDeveloper.bottom
        anchors.topMargin: 5
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignTop
        font.pixelSize: 15
    }

    //***************************************Version************************************//

    AppText {
        id: textVersion
        text: "Version"
        font.underline: true
        anchors.top: infoTextDeveloper.bottom
        anchors.topMargin: 10
        font.family: "Times New Roman"
        width: aboutPage.width
        font.pixelSize: 25
        font.bold: true
        horizontalAlignment: Text.AlignHCenter
    }

    AppText {
        id: infoTextVersion
        width: aboutPage.width
        text: "1.0 -- <b>Release Date</b>: 5/31/18"
        font.family: "Verdana"
        font.italic: true
        anchors.top: textVersion.bottom
        anchors.topMargin: 5
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignTop
        font.pixelSize: 15
    }

} // End of Search Page
