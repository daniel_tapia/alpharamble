import VPlayApps 1.0
import QtQuick 2.7

App {
    id: rambleApp
    screenWidth: 480
    screenHeight: 854

    Navigation {
        id: appNavigation

        NavigationItem {//Home Tab
            id: appNavigationItem
            title: "Home"
            icon: IconType.home

            NavigationStack {
                id: navigationStack

                Page { //Splah Page
                    id: splashPage
                    visible: true
                    anchors.horizontalCenter: rambleApp.horizontalCenter
                    anchors.verticalCenter: rambleApp.verticalCenter
                    backgroundColor: "#7fdbff"

                    AppText { // Title
                        id: titleText
                        y: 60
                        color: "#563517"
                        text: "Alpha Ramble!"
                        font.pixelSize: 60
                        font.family: "Times New Roman"
                        font.italic: true
                        font.bold: true
                        verticalAlignment: Text.AlignTop
                        textFormat: Text.AutoText
                        anchors.horizontalCenter: parent.horizontalCenter
                    }

                    AppImage { //Image
                        id: appImage
                        height: 200
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.horizontalCenter: parent.horizontalCenter
                        source: "../assets/vplay-logo.png"
                        fillMode: Image.PreserveAspectFit
                    }

                }//splashPage
            }//navigationStack
        }//appNavigationItem

//        NavigationItem {//Tab to Search Page
//            id: navigationItem1
//            icon: IconType.barchart
//            visible: true
//            title: "Search"
//            source: Qt.resolvedUrl("SearchPage.qml")
//        }

        NavigationItem {//Tab to Word Page
            id: navigationItem3
            icon: IconType.search
            visible: true
            title: "Word Search"
            source: Qt.resolvedUrl("WordSearch.qml")
        }

        NavigationItem {//Tab to Scrabble Page
            id: navigationItem2
            icon: IconType.calculator
            visible: true
            title: "Scrabble Scorer"
            source: Qt.resolvedUrl("ScrabblePoints.qml")
        }

        NavigationItem {//Tab to Word Page
            id: navigationItem4
            icon: IconType.infocircle
            visible: true
            title: "About Page"
            source: Qt.resolvedUrl("AboutPage.qml")
        }
    }//appNavigation
}//rambleApp
