import VPlayApps 1.0
import QtQuick 2.7
import QtQuick.XmlListModel 2.0

Page {
    id: wordPage
    width: 436
    height: 824
    backgroundColor: "#7fdbff"
    title: "Word Generator"

    // model for loading and parsing xml data
    XmlListModel {
        id: xmlModel

        // set xml source to load data from local file or web service
        //source: Qt.resolvedUrl("data.xml")

        // wordgamedictionary API Key
        // API Endpoint
        property string endpoint: "https://www.wordgamedictionary.com/api/v1/references/scrabble/"
        // Word for Points
        property string word: "dog"

        source: endpoint + word + wordAPIKey

        // set query that returns items
        query: "/entry"

        // specify roles to access item data
        //XmlRole { name: "itemText"; query: "string()" }
        XmlRole { name: "word"; query: "word/string()"}
        XmlRole { name: "scrabblescore"; query: "scrabblescore/string()"}
    }

    //NavigationStack {

        // we display the xml model in a list
        ListPage {
            id: wordListPage
            title: "Parse XML"
            model: xmlModel
            delegate: SimpleRow { text: "Word: " + word + ", Scrabble Score:" + scrabblescore}
        }
    //}
}
